---
layout: default
title: "Création rapide de cartes précises pour les livreurs GLS"
published: true
classes:
 - slide
slide-id: 1
data:
  x: 0
  y: 0

---
<h2>Ou, comment réduire le nombre de colis en problèmes d'adresses.</h2>

<p class="source_text">
  Julien Noblet 2015
</p>
